#!/usr/bin/env python3
'''simsyn.py - simple synthesizer

usage:

  simsyn.py <abc-tune>

  <abc-tune> is a sequence of notes following the abc standard v2.1 (http://abcnotation.com/wiki/abc:standard:v2.1)

author:

  Steffen Brinkmann <s-b@mailbox.org>

copyright:

  © 2018, Steffen Brinkmann, published under the terms of the MIT license (https://opensource.org/licenses/MIT)
'''


from time import sleep
import sys

import numpy as np
from maps import FrozenMap, namedfixedkey
from pygame.time import get_ticks
from pysndfx import AudioEffectsChain

# which library to use for playing sounds
SOUND_METHOD = "pygame"
# maximum volume factor to avoid clipping
MAX_VOL = 0.5
# sample frequency
SAMPLE_FREQ = 44100

# import sound proxy
if SOUND_METHOD == "pygame":
    import pygame
    pygame.mixer.pre_init(SAMPLE_FREQ, size=-16, channels=2, buffer=512)
    pygame.mixer.init()
elif SOUND_METHOD == "sounddevice":
    import sounddevice
elif SOUND_METHOD == "pyaudio":
    import pyaudio
    PYAU = pyaudio.PyAudio()
    STREAM = PYAU.open(rate=SAMPLE_FREQ, format=pyaudio.paInt16, channels=1, output=True)
else:
    raise Exception(f'SOUND_METHOD {SOUND_METHOD} unknown.')

#import matplotlib.pyplot as plt


NOTE = FrozenMap({'C': 130.81,
                  #'C#': 138.59,
                  'D': 146.83,
                  #'D#': 155.56,
                  'E': 164.81,
                  'F': 174.61,
                  #'F#': 185,
                  'G': 196,
                  #'G#': 207.65,
                  'A': 220,
                  #'A#': 233.08,
                  'B': 246.94,
                  'c': 261.63,
                  #'c#': 277.18,
                  'd': 293.66,
                  #'d#': 311.13,
                  'e': 329.63,
                  'f': 349.23,
                  #'f#': 369.99,
                  'g': 392,
                  #'g#': 415.3,
                  'a': 440,
                  #'a#': 466.16,
                  'b': 493.88
                 })

ADSR = namedfixedkey('ADSR',
                     ['attack', 'decay', 'sustain', 'release'],
                     {'attack': 0.02, 'decay': 0.1, 'sustain': 0.7, 'release': 0.1})

OVERTONE = namedfixedkey('OVERTONE',
                         ['rel_freq', 'waveform', 'adsr', 'rel_duration', 'rel_volume'],
                         {'rel_freq': 1.0,
                          'waveform': '\'sin\'',
                          'adsr': '\'ADSR()\'',
                          'rel_duration': 1.0,
                          'rel_volume': 1.0})

INSTRUMENT = namedfixedkey('INSTRUMENT',
                           ['overtones'],
                           {'overtones': ['\'OVERTONE()\'']})

BRASS1 = INSTRUMENT([OVERTONE(1.0, 'sin', ADSR(0.05, 0.35, 0.6, 0.1)),
                     OVERTONE(2.0, 'sin', ADSR(0.035, 0.25, 0.6, 0.05), 0.9, 0.7),
                     OVERTONE(3.0, 'sin', ADSR(0.025, 0.2, 0.6, 0.05), 0.9, 0.7),
                     OVERTONE(4.0, 'sin', ADSR(0.02, 0.15, 0.6, 0.05), 0.9, 0.7)])

BRASS2 = INSTRUMENT([OVERTONE(1.0, 'sin', ADSR(0.04, 0.25, 0.8, 0.25)),
                     OVERTONE(2.0, 'sin', ADSR(0.035, 0.2, 0.6, 0.22), 0.9, 0.7),
                     OVERTONE(3.0, 'sin', ADSR(0.025, 0.2, 0.6, 0.12), 0.9, 0.5),
                     OVERTONE(4.0, 'sin', ADSR(0.02, 0.15, 0.6, 0.12), 0.9, 0.3)])

ORGAN1 = INSTRUMENT([OVERTONE(1.0, 'sin', ADSR(0.02, 0.05, 0.99, 0.15)),
                     OVERTONE(1.008, 'sin', ADSR(0.02, 0.05, 0.99, 0.12), 0.9, 0.9),
                     OVERTONE(2.0, 'sin', ADSR(0.015, 0.02, 0.99, 0.1), 0.9, 0.2),
                     OVERTONE(2.004, 'sin', ADSR(0.015, 0.02, 0.99, 0.1), 0.9, 0.2),
                     OVERTONE(3.0, 'sin', ADSR(0.015, 0.02, 0.99, 0.1), 0.9, 0.33),
                     OVERTONE(5.0, 'sin', ADSR(0.01, 0.015, 0.99, 0.1), 0.9, 0.2),
                     OVERTONE(7.0, 'sin', ADSR(0.01, 0.015, 0.99, 0.1), 0.9, 0.14)])

def apply_adsr(x, adsr=ADSR(), sample_freq=SAMPLE_FREQ):
    '''apply adsr hull'''

    if adsr.attack + adsr.decay > len(x) / sample_freq:
        decay_save = adsr.decay
        adsr.decay = len(x) / sample_freq - adsr.attack
        adsr.sustain = 1.0 - (1.0 - adsr.sustain ) * adsr.decay / decay_save

    # generate hull
    a = np.linspace(0., 1., int(round(adsr.attack * sample_freq)))
    d = np.linspace(1., adsr.sustain, int(round(adsr.decay * sample_freq)))
    s = np.ones(len(x) - int(round(adsr.attack + adsr.decay, 6) * sample_freq)) * adsr.sustain
    r = np.linspace(1., 0., int(round(adsr.release * sample_freq)))



    hull = np.concatenate((a, d, s))
    if hull.shape[0] < x.shape[0]:
        diff = x.shape[0] - hull.shape[0]
        hull = np.concatenate((hull, hull[-1] * np.ones(diff)))
    elif hull.shape[0] > x.shape[0]:
        diff = hull.shape[0] - x.shape[0]
        hull = hull[:-diff]
    # apply hull
    res = (hull * x)
    # get patch from the end to repeat for release
    cutoff_freq = 25
    release_patch = res[-int(1.5*sample_freq / cutoff_freq):]
    while release_patch.shape[0] < int(adsr.release * sample_freq):
        release_patch = cat2(release_patch, release_patch)
    release_patch = release_patch[:int(adsr.release * sample_freq)]
    # append release
    res = cat2(res, r * release_patch)
    return res


def gen_wave(waveform, freq, duration, sample_freq=SAMPLE_FREQ):
    '''generate a simple wave usually as a part of a more complex tone.'''
    if waveform == 'si' or waveform == 'sin' or waveform == 'sine':
        return np.sin(2 * np.pi * freq * np.arange(0, duration, 1/sample_freq))
    elif waveform == "no" or waveform == 'noi' or waveform == 'noise':
        return np.random.rand(SAMPLE_FREQ * duration)
    elif waveform == "hn" or waveform == 'hano' or waveform == 'harmonic-noise':
        return mix(*[np.sin(2 * np.pi * freq * i *
                            np.arange(0, duration, 1/sample_freq) +
                            np.random.rand(duration*SAMPLE_FREQ)*np.pi)
                     for i in np.logspace(0,1,64)])
    else:
        raise NotImplementedError

def gen_tone(freq, duration, instrument):
    '''generate a tone from an instrument'''
    overtone_waves = [apply_adsr(gen_wave(overtone.waveform,
                                          overtone.rel_freq * freq,
                                          overtone.rel_duration * duration),
                                 overtone.adsr) * overtone.rel_volume
                      for overtone in instrument.overtones]
    return mix(*overtone_waves)

def play(x:tuple, max_vol=MAX_VOL, sample_freq=SAMPLE_FREQ):
    '''play a tone'''
    xl, xr = x
    if xl.shape[0] > xr.shape[0]:
        xr = np.concatenate((xr, np.zeros(xl.shape[0] - xr.shape[0])))
    elif xl.shape[0] < xr.shape[0]:
        xl = np.concatenate((xl, np.zeros(xr.shape[0] - xl.shape[0])))

    xl = (xl * max_vol * 32768).astype(np.int16)  # scale to int16 for sound card
    xr = (xr * max_vol * 32768).astype(np.int16)  # scale to int16 for sound card
    x = np.stack((xl, xr), axis=1)

    #plt.plot(res,'x-')
    #plt.grid()
    #plt.show()
    if SOUND_METHOD == "sounddevice":
        sounddevice.play(x, sample_freq)
    elif SOUND_METHOD == "pygame":
        sound = pygame.sndarray.make_sound(x)
        sound.play()
    elif SOUND_METHOD == "pyaudio":
        STREAM.write(x.tobytes())


def mix(*tones):
    '''mixes an arbitrary number of tones'''
    max_len = max(map(len, tones))

    res = []
    for tone in tones:
        tone_cp = tone.copy()
        tone_cp.resize((max_len,))
        res.append(tone_cp)

    res = np.mean(res, axis=0)
    return res

# TODO: def cut_to_root(tone, left=True, right=True):

def cat2(tone0, tone1):
    '''concatenates two tones avoiding discontinuities'''
    idx = jdx = 1
    for i in range(1, len(tone0)):
        if tone0[-i] * tone0[-i-1] < 0:
            idx = i
            break
    else: raise Exception('no root found, unable to concatenate')

    deriv_pos = tone0[-idx] - tone0[-idx-1] > 0

    for j, _ in enumerate(tone1):
        if (tone1[j] * tone1[j+1] < 0 and
                (tone1[j+1] - tone1[j] > 0) == deriv_pos):
            jdx = j
            break
    else: raise Exception('no root found, unable to concatenate')

    return np.concatenate((tone0[:-idx], tone1[jdx+1:]))


def get_tune_length(tune_body, t_measure):
    '''calculate the tune length in s from the abc tune data'''
    tune_body.strip()
    tune_body.replace('||', '|')
    measure_count = tune_body.count('|') + 1
    if tune_body[0] == '|': measure_count -= 1
    if tune_body[-1] == '|': measure_count -= 1

    return measure_count * t_measure

def place_tone(tune, tone, time, sample_freq=SAMPLE_FREQ):
    '''place a tone into a tune at a given time'''
    assert len(tune) - len(tone) > time * sample_freq

    time_idx = int(time * sample_freq)
    tune[time_idx:time_idx + len(tone)] += tone

    return tune

if __name__ == '__main__':

    TUNE = sys.argv[1]
    print('tune:', TUNE)
    if not TUNE:
        sys.exit(1)

    # tempo
    BPM = 180
    # meter
    METER = '4/4'
    QUARTER_PER_MEASURE = 4 * int(METER.split('/')[0]) / int(METER.split('/')[1])
    # duration of notes
    T_WHOLE = T_1 = 60 / (BPM / 4)
    T_HALF = T_2 = 60 / (BPM / 2)
    T_QUARTER = T_4 = 60 / BPM
    T_EIGHTH = T_8 = 60 / (BPM * 2)
    T_SIXTEENTH = T_16 = 60 / (BPM * 4)
    T_MEASURE = T_4 * QUARTER_PER_MEASURE

    #print(T_1, T_2, T_4, T_8, T_16)

    T_UNIT = T_4

    # prepare result tune as silence of sufficient length
    result_tune = np.zeros(int(get_tune_length(TUNE, T_MEASURE) * SAMPLE_FREQ) + 2*SAMPLE_FREQ)
    print(f'Calculated length of resulting tune: {len(result_tune) / SAMPLE_FREQ}')
    print(f'Start playing the tune after {get_ticks()} ms.')

    current_t = 0
    for i, ch in enumerate(TUNE):
        if ch not in 'ABCDEFGabcdefgz':
            continue

        # note length
        multiplier = 1
        if i < len(TUNE)-1 and TUNE[i+1] in '1234567890':
            if i < len(TUNE)-2 and TUNE[i+2] in '1234567890':
                multiplier = int(10 * TUNE[i+1]+TUNE[i+2])
            else:
                multiplier = int(TUNE[i+1])
        elif i < len(TUNE)-1 and TUNE[i+1] == '/':
            if i < len(TUNE)-2 and TUNE[i+2] in '1234567890':
                if i < len(TUNE)-3 and TUNE[i+3] in '1234567890':
                    multiplier = 1/int(10 * TUNE[i+2] + TUNE[i+3])
                else:
                    multiplier = 1/int(TUNE[i+2])
            else:
                multiplier = 1/2

        T_TONE = T_UNIT * multiplier

        # rests
        if ch == 'z':
            current_t += T_TONE
            continue

        # tone frequency
        freq = NOTE[ch]

        x0 = gen_tone(freq, T_TONE, ORGAN1)
        x1 = gen_tone(freq*.333, T_TONE, ORGAN1)

        #result_tune = place_tone(result_tune, x0, current_t)
        #current_t += T_TONE
        play((x0, x1))
        sleep(T_TONE)

    # apply SoX effects
    fx = (AudioEffectsChain()
          .gain(-2)
          #.highpass(100)
          #.chorus(0.4, 0.6, [[55, 0.4, 0.55, .5, 't']])
          #.lowpass(10000)
          .delay(0.7, 0.8, [31, 57], [.8,.7])
          #.delay(0.7, 0.8, [31, 57], [.8,.7])
          #.gain(-2)
          .reverb(reverberance=60,
                  hf_damping=40,
                  room_scale=100,
                  stereo_depth=100,
                  pre_delay=60,
                  wet_gain=0)
    )
    result_tune = fx(result_tune)

    # play complete tune
    # play(result_tune)
    # sleep(2)
